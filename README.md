# Code-Canteen

A set of links to public resources designed to help you to learn to code, to improve your coding skills, or to challenge yourself.  

This information is designed to be accessed by users of Code Canteen, an online place where you can learn at a scheduled time and with others.  It is designed to combine the benefits of online learning with the benefits of social learning.  We believe that we learn best when we learn with others and that we never stop learning.  

The Code Canteen opens twice a week to give you a scheduled time to learn to code and to learn with others.  

## About this Event

### What is Code Canteen? 

Code Canteen is a regular opportunity for people to get together and code. Inclusive to all, we hope to encourage people to learn something new, share knowledge, work on our projects, share code problems and help others.  

### Who will benefit from attending Code Canteen?

Code Canteen is for you if:  

* you have just started learning with online tutorials - you can work through these at the Code Canteen and know that others are there to discuss issues and to help answer questions
* you are already programming and thinking about a career in software development, or about improving your skills - we have a range of projects for not-for-profits, for which we are looking for people to develop open source code - we anticipate being able to identify suitable candidates for software development apprenticeships through Code Canteen.
* you are a developer and want to work on programming and problem solving challenges with other - you can find others who want to share the challenge (or work on your own) and use the Code Canteen time as a goal for completing the challenge
* you are a developer and you want to help others - we are looking to build a community of mentors who can support others in their learning. Mentoring is a great way to give a little back to the community and it really rewarding.

### How does it work?

This is an online meetup hosted on Slack, starting with introductions and your aims for the session in the main event-channel. Then you will join a channel with others who share similar goals so that you can support and work with each other. Ideally each group will have a mentor who can help lead the session. At the end of the session, we will all join back together to talk about what we have achieved.  

Booking is required for each session you want to attend, as places are limited. Once you’ve register for your first session , we will send you a link to the Slack channel and your account will be activated at the beginning of the session. For subsequent sessions, your link will become active again at the start of every session you have specifically booked.  

## FAQs

*Are there ID or minimum age requirements to join a Code Canteen event?*  

All attendees will be 18+  

*How can I contact the organiser with any questions?*  

Please send queries to: karen@futurecoders.org.uk  

*Should I prepare anything before Code Canteen?*  

You'll get more out of Code Canteen if you have a good idea of what you want to work on, whether that be a particular language, tutorial or a personal project. However, if you know you want to learn but don't know where to start we can help you with finding learning resources to get you going.  The [current menu is here](url) for you to browse.  

Due to all of the sessions being online, we recommend a good microphone and camera with no background interference.  
